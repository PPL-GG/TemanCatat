package com.ppl.d4.temancatat;

import com.ppl.d4.temancatat.models.Example;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class ExampleUnitTest {

    @Test
    public void testExampleNumberAttribute() throws Exception {
        Example example = new Example();
        example.setNumber(5);
        assertTrue(example.getNumber() == 5);
    }

    @Test
    public void testExampleSentenceAttribute() throws Exception {
        Example example = new Example();
        example.setSentence("Anab Ganteng");
        assertTrue(example.getSentence().equals("Anab Ganteng"));
    }
}
