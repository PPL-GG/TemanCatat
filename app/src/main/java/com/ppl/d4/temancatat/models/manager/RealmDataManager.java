package com.ppl.d4.temancatat.models.manager;

import com.ppl.d4.temancatat.models.Example;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Class to communicate with realm database manager
 */
public class RealmDataManager {

    private final Realm mRealm;

    public RealmDataManager(final Realm realm) {
        mRealm = realm;
    }

    public void closeRealm() {
        mRealm.close();
    }

    public void addExample(final int number,
                           final String sentence,
                           final OnTransactionCallback onTransactionCallback) {
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(final Realm realm) {
                Example example = realm.createObject(Example.class);
                example.setNumber(number);
                example.setSentence(sentence);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                if (onTransactionCallback != null) {
                    onTransactionCallback.onRealmSuccess();
                }
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                if (onTransactionCallback != null) {
                    onTransactionCallback.onRealmError(error);
                }
            }
        });
    }

    public RealmResults<Example> getExamples() {
        return mRealm.where(Example.class).findAll();
    }

    public void deleteExamples(final OnTransactionCallback onTransactionCallback) {
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(final Realm realm) {
                RealmResults<Example> examples = realm.where(Example.class).findAll();
                examples.deleteAllFromRealm();
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                if (onTransactionCallback != null) {
                    onTransactionCallback.onRealmSuccess();
                }
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                if (onTransactionCallback != null) {
                    onTransactionCallback.onRealmError(error);
                }
            }
        });
    }

    public interface OnTransactionCallback {
        void onRealmSuccess();

        void onRealmError(Throwable e);
    }
}