package com.ppl.d4.temancatat.di.modules;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.ppl.d4.temancatat.di.ActivityContext;
import com.ppl.d4.temancatat.di.PerActivity;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.implementations.MainPresenter;
import com.ppl.d4.temancatat.presenters.interfaces.MainPresenterInterface;
import com.ppl.d4.temancatat.ui.views.MainView;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

/**
 * Class to provide modules to application.
 * Also used to provide presenter to activities.
 * Don't forget to provide newly added presenter in this class.
 */
@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    @PerActivity
    MainPresenterInterface<MainView> provideMainPresenter(
            MainPresenter<MainView> presenter) {
        return presenter;
    }

    @Provides
    Realm provideRealm() {
        return Realm.getDefaultInstance();
    }

    @Provides
    RealmDataManager provideRealmService(final Realm realm) {
        return new RealmDataManager(realm);
    }
}