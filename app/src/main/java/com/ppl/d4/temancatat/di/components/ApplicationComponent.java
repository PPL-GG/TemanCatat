package com.ppl.d4.temancatat.di.components;

import android.content.Context;

import com.ppl.d4.temancatat.Application;
import com.ppl.d4.temancatat.di.ApplicationContext;
import com.ppl.d4.temancatat.di.modules.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(Application app);

    @ApplicationContext
    Context context();

    android.app.Application application();
}
