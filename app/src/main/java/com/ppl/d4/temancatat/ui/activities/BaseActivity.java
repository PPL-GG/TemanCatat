package com.ppl.d4.temancatat.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ppl.d4.temancatat.Application;
import com.ppl.d4.temancatat.di.components.ActivityComponent;
import com.ppl.d4.temancatat.di.components.DaggerActivityComponent;
import com.ppl.d4.temancatat.di.modules.ActivityModule;
import com.ppl.d4.temancatat.ui.views.BaseView;

import butterknife.Unbinder;

/**
 * Class that will render the UI.
 * Activity will implement corresponding view interface.
 * Other activities have to extends BaseActivity.
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    private ActivityComponent mActivityComponent;

    private Unbinder mUnBinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((Application) getApplication()).getApplicationComponent())
                .build();
    }

    public ActivityComponent getActivityComponent() {
        return this.mActivityComponent;
    }

    public void setUnBinder(Unbinder unBinder) {
        this.mUnBinder = unBinder;
    }

    @Override
    protected void onDestroy() {

        if (mUnBinder != null) {
            mUnBinder.unbind();
        }

        closeRealm();

        super.onDestroy();
    }

    protected abstract void closeRealm();
}
