package com.ppl.d4.temancatat.ui.activities;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.ppl.d4.temancatat.R;
import com.ppl.d4.temancatat.presenters.interfaces.MainPresenterInterface;
import com.ppl.d4.temancatat.ui.views.MainView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainView {

    private static final String TAG = "MainActivity";

    @Inject
    MainPresenterInterface<MainView> mPresenter;

    @BindView(R.id.hello_world_textview)
    TextView helloWorldTextView;

    @Override
    @OnClick(R.id.hello_world_textview)
    public void onClickHelloWorldText() {
        mPresenter.getExamples();
        Toast.makeText(this, "hello world clicked", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        ButterKnife.bind(this);

        mPresenter.onAttach(this);

        helloWorldTextView.setTextColor(this.getResources().getColor(R.color.colorAccent));
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void closeRealm() {
        mPresenter.closeRealm();
    }
}
