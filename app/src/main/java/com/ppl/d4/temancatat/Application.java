package com.ppl.d4.temancatat;

import com.ppl.d4.temancatat.di.components.ApplicationComponent;
import com.ppl.d4.temancatat.di.components.DaggerApplicationComponent;
import com.ppl.d4.temancatat.di.modules.ApplicationModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class Application extends android.app.Application{

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();

        mApplicationComponent.inject(this);

        initRealmConfiguration();
    }

    private void initRealmConfiguration() {
        Realm.init(getApplicationContext());

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}
