package com.ppl.d4.temancatat.presenters.interfaces;

import com.ppl.d4.temancatat.ui.views.MainView;

public interface MainPresenterInterface<V extends MainView> extends BasePresenterInterface<V> {

    void getExamples();
}
