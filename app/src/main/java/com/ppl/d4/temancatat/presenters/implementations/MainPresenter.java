package com.ppl.d4.temancatat.presenters.implementations;

import android.util.Log;

import com.ppl.d4.temancatat.models.Example;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.interfaces.MainPresenterInterface;
import com.ppl.d4.temancatat.ui.views.MainView;

import javax.inject.Inject;

import io.realm.RealmResults;

public class MainPresenter<V extends MainView> extends BasePresenter<V> implements MainPresenterInterface<V>, RealmDataManager.OnTransactionCallback {

    private static final String TAG = "MainPresenter";

    @Inject
    public MainPresenter(final RealmDataManager realmDataManager) { super(realmDataManager); }

    @Override
    public void getExamples() {
        RealmResults<Example> examples = getRealmDataManager().getExamples();
        if (examples.size() == 0) {
            getRealmDataManager().addExample(1, "Anab Ganteng", this);
        }
        else {
            Log.d(TAG, "udh ada example" + examples.last().getSentence());
            getRealmDataManager().deleteExamples(this);
        }
    }

    @Override
    public void closeRealm() {
        super.closeRealm();
    }

    @Override
    public void onRealmSuccess() {
        Log.d(TAG, "realm transaction success");
    }

    @Override
    public void onRealmError(Throwable e) {
        Log.d(TAG, "realm transaction error" + e.toString());
    }
}