package com.ppl.d4.temancatat.presenters.interfaces;

import com.ppl.d4.temancatat.ui.views.BaseView;

/**
 * Interface that is implemented by presenter.
 * Contains method given to its Activity.
 * One-to-one relationship between presenter.
 * Other presenter interface have to extend BasePresenterInterface
 * @param <V>
 */
public interface BasePresenterInterface<V extends BaseView> {

    void onAttach(V view);

    void onDetach();

    void closeRealm();
}