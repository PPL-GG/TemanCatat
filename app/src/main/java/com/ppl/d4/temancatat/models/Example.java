package com.ppl.d4.temancatat.models;

import io.realm.RealmObject;

public class Example extends RealmObject {
    private int number;
    private String sentence;

    public void setNumber(int number) {
        this.number = number;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public int getNumber() {

        return number;
    }

    public String getSentence() {
        return sentence;
    }
}
