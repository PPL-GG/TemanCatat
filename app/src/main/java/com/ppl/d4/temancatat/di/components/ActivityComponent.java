package com.ppl.d4.temancatat.di.components;

import com.ppl.d4.temancatat.di.PerActivity;
import com.ppl.d4.temancatat.di.modules.ActivityModule;
import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.ui.activities.MainActivity;

import dagger.Component;

/**
 * Class used to specify which component to be injected by the modules.
 * Don't forget to add newly added activities in this class.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    void inject(RealmDataManager realmDataManager);
}