package com.ppl.d4.temancatat.presenters.implementations;

import com.ppl.d4.temancatat.models.manager.RealmDataManager;
import com.ppl.d4.temancatat.presenters.interfaces.BasePresenterInterface;
import com.ppl.d4.temancatat.ui.views.BaseView;

import javax.inject.Inject;

/**
 * Class that will do the logic of the UI.
 * Also place to inject external dependencies to activity.
 * Presenter will implement corresponding presenter interface.
 * One-to-one relationship between activity.
 * Other presenter have to extend BasePresenter
 * @param <V>
 */
public class BasePresenter<V extends BaseView> implements BasePresenterInterface<V> {

    private static final String TAG = "BasePresenter";

    private final RealmDataManager mRealmDataManager;

    private V view;

    @Inject
    public BasePresenter(RealmDataManager realmDataManager) {
        this.mRealmDataManager = realmDataManager;
    }

    public RealmDataManager getRealmDataManager() {
        return this.mRealmDataManager;
    }

    @Override
    public void onAttach(V view) {
        this.view = view;
    }

    @Override
    public void onDetach() {}

    public V getView() {
        return view;
    }

    @Override
    public void closeRealm() {
        mRealmDataManager.closeRealm();
    }
}
